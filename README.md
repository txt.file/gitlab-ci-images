<!-- SPDX-License-Identifier: Unlicense -->
<!-- SPDX-FileCopyrightText: 2022 Vieno Hakkerinen <txt.file@txtfile.eu> -->

# GitLab CI images

Build and provide images for GitLab CI runner.

## Licenses

All files have a proper [REUSE](https://reuse.software/) license & copyright
header.
